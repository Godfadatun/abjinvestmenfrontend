
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { name:'home',path: '', component: () => import('pages/Index.vue'),
      children: [
        { name: 'login', path: 'login'},
        { name: 'register', path: 'register'},
        { name: 'forget', path: 'forget'},
        { name: 'how', path: 'how-it-works'},
      ]
    },
      { name: 'transaction', path: 'transaction', component: () => import('pages/transaction.vue') },
      { name: 'investments', path: 'investments', component: () => import('pages/investments.vue') },
      { name: 'packages', path: 'packages', component: () => import('pages/packages.vue') },
      { name: 'dashboard', path: 'dashboard', component: () => import('pages/dashboard.vue') },
      { name: 'settings', path: 'settings', component: () => import('pages/settings.vue') },

    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
