export const authRuleMixin = {
    methods: {
        textRule(val){
         return (val && val.length > 0 || 'Please type something')
        },
    
        textLengthRule(val){
         return val.length >= 6 || 'Please use minimum of 6 characters'
        },
    
        textLengthRule1(val){
         return val.length >= 10 || 'Please use minimum of 10 characters'
        },

        validEmail: function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }
  },

  computed: {
    isValid(){
      return this.form.password_confirmation == this.form.password;
    }
  }

}