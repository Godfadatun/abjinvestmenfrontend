import { LocalStorage } from "quasar";
export function login (state, data) {
  LocalStorage.set('abjinvestment-token', data.token);
  state.token = data.token;
  state.user = data.data;
}

